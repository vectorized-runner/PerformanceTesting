using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.PerformanceTesting;
using Random = UnityEngine.Random;

namespace PerformanceTests
{
	public struct DataA : IComponentData
	{
		public float Value;
	}

	public struct DataB : IComponentData
	{
		public float Value;
	}

	public static class Computation
	{
		public static void DoTheWork(ref DataA dataA, in DataB dataB)
		{
			dataA.Value += math.PI * math.sqrt(math.unlerp(math.cos(math.ceil(dataB.Value)), -1, 1));
		}
	}

	[DisableAutoCreation]
	[AlwaysUpdateSystem]
	public class RunSystem : SystemBase
	{
		protected override void OnUpdate()
		{
			Entities.ForEach((ref DataA dataA, in DataB dataB) => { Computation.DoTheWork(ref dataA, dataB); })
			        .Run();
		}
	}

	[DisableAutoCreation]
	[AlwaysUpdateSystem]
	public class ScheduleSystem : SystemBase
	{
		protected override void OnUpdate()
		{
			Entities.ForEach((ref DataA dataA, in DataB dataB) => { Computation.DoTheWork(ref dataA, dataB); })
			        .Schedule();
		}
	}

	[DisableAutoCreation]
	[AlwaysUpdateSystem]
	public class ScheduleParallelSystem : SystemBase
	{
		protected override void OnUpdate()
		{
			Entities.ForEach((ref DataA dataA, in DataB dataB) => { Computation.DoTheWork(ref dataA, dataB); })
			        .ScheduleParallel();
		}
	}

	[DisableAutoCreation]
	[AlwaysUpdateSystem]
	public class NoBurstSystem : SystemBase
	{
		protected override void OnUpdate()
		{
			Entities.ForEach((ref DataA dataA, in DataB dataB) => { Computation.DoTheWork(ref dataA, dataB); })
			        .WithoutBurst()
			        .Run();
		}
	}

	[BurstCompile]
	public static unsafe class EntitiesSystemRunningTypeBenchmarks
	{
		const int IterationCount = 10;
		const int WorldUpdateCount = 10;

		[Test, Performance]
		public static void UpdateEmptyWorld()
		{
			World world = default;

			Measure.Method(() =>
			       {
				       for(int i = 0; i < WorldUpdateCount; i++)
				       {
					       ECSTestUtil.TickWorld(world);
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .SetUp(() => { world = ECSTestUtil.CreateEditorWorld(); })
			       .CleanUp(() => { world.Dispose(); })
			       .Run();
		}

		[Test, Performance]
		public static void RunSystemTest_10kEntities()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       for(int i = 0; i < WorldUpdateCount; i++)
				       {
					       ECSTestUtil.TickWorld(world);
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       ECSTestUtil.AddSystemToWorld<RunSystem>(world);
				       ECSTestUtil.CreateEntities(world, 10000, Allocator.Temp, typeof(DataA), typeof(DataB));

				       for(int i = 0; i < entities.Length; i++)
				       {
					       ECSTestUtil.SetComponent(world, entities[i], new DataA { Value = Random.value });
					       ECSTestUtil.SetComponent(world, entities[i], new DataB { Value = Random.value });
				       }
			       })
			       .CleanUp(() => { world.Dispose(); })
			       .Run();
		}

		[Test, Performance]
		public static void NoBurstSystemTest_10kEntities()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       for(int i = 0; i < WorldUpdateCount; i++)
				       {
					       ECSTestUtil.TickWorld(world);
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       ECSTestUtil.AddSystemToWorld<NoBurstSystem>(world);
				       ECSTestUtil.CreateEntities(world, 10000, Allocator.Temp, typeof(DataA), typeof(DataB));

				       for(int i = 0; i < entities.Length; i++)
				       {
					       ECSTestUtil.SetComponent(world, entities[i], new DataA { Value = Random.value });
					       ECSTestUtil.SetComponent(world, entities[i], new DataB { Value = Random.value });
				       }
			       })
			       .CleanUp(() => { world.Dispose(); })
			       .Run();
		}

		[Test, Performance]
		public static void ScheduleParallelSystemTest_10kEntities()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       for(int i = 0; i < WorldUpdateCount; i++)
				       {
					       ECSTestUtil.TickWorld(world);
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       ECSTestUtil.AddSystemToWorld<ScheduleParallelSystem>(world);
				       ECSTestUtil.CreateEntities(world, 10000, Allocator.Temp, typeof(DataA), typeof(DataB));

				       for(int i = 0; i < entities.Length; i++)
				       {
					       ECSTestUtil.SetComponent(world, entities[i], new DataA { Value = Random.value });
					       ECSTestUtil.SetComponent(world, entities[i], new DataB { Value = Random.value });
				       }
			       })
			       .CleanUp(() => { world.Dispose(); })
			       .Run();
		}

		[Test, Performance]
		public static void ScheduleSystemTest_10kEntities()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       for(int i = 0; i < WorldUpdateCount; i++)
				       {
					       ECSTestUtil.TickWorld(world);
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       ECSTestUtil.AddSystemToWorld<ScheduleSystem>(world);
				       ECSTestUtil.CreateEntities(world, 10000, Allocator.Temp, typeof(DataA), typeof(DataB));

				       for(int i = 0; i < entities.Length; i++)
				       {
					       ECSTestUtil.SetComponent(world, entities[i], new DataA { Value = Random.value });
					       ECSTestUtil.SetComponent(world, entities[i], new DataB { Value = Random.value });
				       }
			       })
			       .CleanUp(() => { world.Dispose(); })
			       .Run();
		}

		[Test, Performance]
		public static void RunSystemTest_100Entities()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       for(int i = 0; i < WorldUpdateCount; i++)
				       {
					       ECSTestUtil.TickWorld(world);
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       ECSTestUtil.AddSystemToWorld<RunSystem>(world);
				       ECSTestUtil.CreateEntities(world, 100, Allocator.Temp, typeof(DataA), typeof(DataB));

				       for(int i = 0; i < entities.Length; i++)
				       {
					       ECSTestUtil.SetComponent(world, entities[i], new DataA { Value = Random.value });
					       ECSTestUtil.SetComponent(world, entities[i], new DataB { Value = Random.value });
				       }
			       })
			       .CleanUp(() => { world.Dispose(); })
			       .Run();
		}

		[Test, Performance]
		public static void RunSystemTest_1kEntities()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       for(int i = 0; i < WorldUpdateCount; i++)
				       {
					       ECSTestUtil.TickWorld(world);
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       ECSTestUtil.AddSystemToWorld<RunSystem>(world);
				       ECSTestUtil.CreateEntities(world, 1000, Allocator.Temp, typeof(DataA), typeof(DataB));

				       for(int i = 0; i < entities.Length; i++)
				       {
					       ECSTestUtil.SetComponent(world, entities[i], new DataA { Value = Random.value });
					       ECSTestUtil.SetComponent(world, entities[i], new DataB { Value = Random.value });
				       }
			       })
			       .CleanUp(() => { world.Dispose(); })
			       .Run();
		}
	}
}