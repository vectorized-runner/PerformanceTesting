using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.PerformanceTesting;
using Unity.Transforms;

namespace PerformanceTests
{
	[BurstCompile]
	public static class EntityManagerVsECBBenchmark
	{
		const int EntityCount = 10000;

		[Test, Performance]
		public static void AddComponentEntityManagerSingle()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       for(int i = 0; i < EntityCount; i++)
				       {
					       world.EntityManager.AddComponent(entities[i], typeof(Translation));
				       }
			       })
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       entities = world.EntityManager.Instantiate(world.EntityManager.CreateEntity(), EntityCount,
					       Allocator.Temp);
			       })
			       .CleanUp(() =>
			       {
				       world.Dispose();
				       entities.Dispose();
			       })
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10)
			       .WarmupCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void AddComponentEntityManagerQuery()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       world.EntityManager.AddComponent(world.EntityManager.UniversalQuery, typeof(Translation));
			       })
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       entities = world.EntityManager.Instantiate(world.EntityManager.CreateEntity(), EntityCount,
					       Allocator.Temp);
			       })
			       .CleanUp(() =>
			       {
				       world.Dispose();
				       entities.Dispose();
			       })
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10)
			       .WarmupCount(10)
			       .Run();
		}

		[BurstCompile]
		struct AddComponentEntityManagerJob : IJob
		{
			public NativeArray<Entity> Entities;
			public EntityManager EntityManager;

			public void Execute()
			{
				for(int i = 0; i < Entities.Length; i++)
				{
					EntityManager.AddComponent(Entities[i], ComponentType.ReadOnly<Translation>());
				}
			}
		}
		
		[BurstCompile]
		struct AddComponentEntityManagerQueryJob : IJob
		{
			public EntityManager EntityManager;

			public void Execute()
			{
				EntityManager.AddComponent(EntityManager.UniversalQuery, ComponentType.ReadOnly<Translation>());
			}
		}
		
		[Test, Performance]
		public static void AddComponentEntityManagerQueryWithJob()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       new AddComponentEntityManagerQueryJob
				       {
					       EntityManager = world.EntityManager
				       }.Run();
			       })
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       entities = world.EntityManager.Instantiate(world.EntityManager.CreateEntity(), EntityCount,
					       Allocator.TempJob);
			       })
			       .CleanUp(() =>
			       {
				       world.Dispose();
				       entities.Dispose();
			       })
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10)
			       .WarmupCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void AddComponentEntityManagerSingleJob()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       new AddComponentEntityManagerJob
				       {
					       Entities = entities,
					       EntityManager = world.EntityManager
				       }.Run();
			       })
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       entities = world.EntityManager.Instantiate(world.EntityManager.CreateEntity(), EntityCount,
					       Allocator.TempJob);
			       })
			       .CleanUp(() =>
			       {
				       world.Dispose();
				       entities.Dispose();
			       })
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10)
			       .WarmupCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void AddComponentECBSingle()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       var commandBuffer = new EntityCommandBuffer(Allocator.Temp, PlaybackPolicy.SinglePlayback);

				       for(int i = 0; i < EntityCount; i++)
				       {
					       commandBuffer.AddComponent(entities[i], typeof(Translation));
				       }

				       commandBuffer.Playback(world.EntityManager);
			       })
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       entities = world.EntityManager.Instantiate(world.EntityManager.CreateEntity(), EntityCount,
					       Allocator.Temp);
			       })
			       .CleanUp(() =>
			       {
				       world.Dispose();
				       entities.Dispose();
			       })
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10)
			       .WarmupCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void AddComponentECBQuery()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       var commandBuffer = new EntityCommandBuffer(Allocator.Temp, PlaybackPolicy.SinglePlayback);
				       commandBuffer.AddComponent(world.EntityManager.UniversalQuery, typeof(Translation));
				       commandBuffer.Playback(world.EntityManager);
			       })
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       entities = world.EntityManager.Instantiate(world.EntityManager.CreateEntity(), EntityCount,
					       Allocator.Temp);
			       })
			       .CleanUp(() =>
			       {
				       world.Dispose();
				       entities.Dispose();
			       })
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10)
			       .WarmupCount(10)
			       .Run();
		}

		[BurstCompile]
		struct AddComponentECBJob : IJob
		{
			public NativeArray<Entity> Entities;
			public EntityCommandBuffer ECB;

			public void Execute()
			{
				for(int i = 0; i < Entities.Length; i++)
				{
					ECB.AddComponent(Entities[i], ComponentType.ReadOnly<Translation>());
				}
			}
		}

		[Test, Performance]
		public static void AddComponentECBSingleJob()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() =>
			       {
				       var ecb = new EntityCommandBuffer(Allocator.TempJob, PlaybackPolicy.SinglePlayback);

				       new AddComponentECBJob
				       {
					       ECB = ecb,
					       Entities = entities,
				       }.Run();

				       ecb.Playback(world.EntityManager);
				       ecb.Dispose();
			       })
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       entities = world.EntityManager.Instantiate(world.EntityManager.CreateEntity(), EntityCount,
					       Allocator.TempJob);
			       })
			       .CleanUp(() =>
			       {
				       world.Dispose();
				       entities.Dispose();
			       })
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10)
			       .WarmupCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void AddComponentEntityManagerArray()
		{
			World world = default;
			NativeArray<Entity> entities = default;

			Measure.Method(() => { world.EntityManager.AddComponent(entities, typeof(Translation)); })
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       entities = world.EntityManager.Instantiate(world.EntityManager.CreateEntity(), EntityCount,
					       Allocator.Temp);
			       })
			       .CleanUp(() =>
			       {
				       world.Dispose();
				       entities.Dispose();
			       })
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10)
			       .WarmupCount(10)
			       .Run();
		}

		struct Group1 : IComponentData
		{
		}

		struct Group2 : IComponentData
		{
		}

		[Test]
		public static void EnsureResultsAreSame()
		{
			var world = ECSTestUtil.CreateEditorWorld();
			var em = world.EntityManager;
			var entity = em.CreateEntity();
			var group1 = em.Instantiate(entity, 1000, Allocator.Temp);
			var group2 = em.Instantiate(entity, 1000, Allocator.Temp);

			em.AddComponent<Group1>(group1);
			em.AddComponent<Group2>(group2);

			for(int i = 0; i < 1000; i++)
			{
				Assert.IsTrue(em.HasComponent<Group1>(group1[i]));
			}

			for(int i = 0; i < 1000; i++)
			{
				Assert.IsTrue(em.HasComponent<Group2>(group2[i]));
			}

			var ecb = new EntityCommandBuffer(Allocator.Temp, PlaybackPolicy.SinglePlayback);
			world.EntityManager.AddComponent(world.EntityManager.CreateEntityQuery(typeof(Group1)),
				typeof(Translation));
			ecb.AddComponent(world.EntityManager.CreateEntityQuery(typeof(Group2)), typeof(Translation));
			ecb.Playback(world.EntityManager);

			for(int i = 0; i < 1000; i++)
			{
				Assert.IsTrue(em.HasComponent<Translation>(group1[i]));
			}

			for(int i = 0; i < 1000; i++)
			{
				Assert.IsTrue(em.HasComponent<Translation>(group2[i]));
			}
		}
	}
}