using System.Diagnostics;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.PerformanceTesting;
using Random = Unity.Mathematics.Random;
using static Unity.Burst.CompilerServices.Hint;

namespace PerformanceTests
{
	[BurstCompile]
	public static class LikelyIntrinsicBenchmarks
	{
		public const float SomeVeryLowChance = 0.01f;
		public const int IterationCount = 10;
		public const int Count = 25000;

		[BurstCompile]
		struct ArraySumJob : IJob
		{
			public NativeList<float> List;
			public float Result;

			public void Execute()
			{
				if(List.Length > 0)
				{
					for(int i = 0; i < List.Length; i++)
					{
						Result += List[i];
					}
				}
			}
		}

		[BurstCompile]
		struct ArraySumIntrinsicsJob : IJob
		{
			public NativeList<float> List;
			public float Result;

			public void Execute()
			{
				if(Likely(List.Length > 0))
				{
					for(int i = 0; i < List.Length; i++)
					{
						Result += List[i];
					}
				}
			}
		}

		[Test, Performance]
		public static void ArraySumNoIntrinsics()
		{
			NativeList<float> list = default;

			Measure.Method(() => { new ArraySumJob { List = list }.Run(); })
			       .SetUp(() =>
			       {
				       list = new NativeList<float>(1000, Allocator.TempJob);
				       if(UnityEngine.Random.value > 0.001f)
				       {
					       for(int i = 0; i < 1000; i++)
					       {
						       list.Add(UnityEngine.Random.value);
					       }
				       }
			       })
			       .CleanUp(() => list.Dispose())
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(100)
			       .Run();
		}

		[Test, Performance]
		public static void ArraySumIntrinsics()
		{
			NativeList<float> list = default;

			Measure.Method(() => { new ArraySumIntrinsicsJob { List = list }.Run(); })
			       .SetUp(() =>
			       {
				       list = new NativeList<float>(1000, Allocator.TempJob);
				       if(UnityEngine.Random.value > 0.001f)
				       {
					       for(int i = 0; i < 1000; i++)
					       {
						       list.Add(UnityEngine.Random.value);
					       }
				       }
			       })
			       .CleanUp(() => list.Dispose())
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(100)
			       .Run();
		}

		[Test, Performance]
		public static void DoComputationDefault()
		{
			Measure.Method(() =>
			       {
				       var random = new Random((uint)Stopwatch.GetTimestamp());
				       var value = DoComputationDefaultMethod(ref random, Count);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[Test, Performance]
		public static void DoComputationLikely()
		{
			Measure.Method(() =>
			       {
				       var random = new Random((uint)Stopwatch.GetTimestamp());
				       var value = DoComputationLikelyMethod(ref random, Count);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[Test, Performance]
		public static void DoComputationFalseUnlikely()
		{
			Measure.Method(() =>
			       {
				       var random = new Random((uint)Stopwatch.GetTimestamp());
				       var value = DoComputationFalseUnlikelyMethod(ref random, Count);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[BurstCompile]
		public static float DoComputationFalseUnlikelyMethod(ref Random random, int count)
		{
			var value = 0f;

			for(int i = 0; i < count; i++)
			{
				if(Unlikely(random.NextFloat() > SomeVeryLowChance))
				{
					value += math.PI * math.acos(math.sqrt(value) * math.sign(value) * math.tanh(value));
				}
				else
				{
					value -= math.PI * math.acos(math.sqrt(value) * math.sign(value) * math.tanh(value));
				}
			}

			return value;
		}

		[BurstCompile]
		public static float DoComputationLikelyMethod(ref Random random, int count)
		{
			var value = 0f;

			for(int i = 0; i < count; i++)
			{
				if(Likely(random.NextFloat() > SomeVeryLowChance))
				{
					value += math.PI * math.acos(math.sqrt(value) * math.sign(value) * math.tanh(value));
				}
				else
				{
					value -= math.PI * math.acos(math.sqrt(value) * math.sign(value) * math.tanh(value));
				}
			}

			return value;
		}

		[BurstCompile]
		public static float DoComputationDefaultMethod(ref Random random, int count)
		{
			var value = 0f;

			for(int i = 0; i < count; i++)
			{
				if(random.NextFloat() > SomeVeryLowChance)
				{
					value += math.PI * math.acos(math.sqrt(value) * math.sign(value) * math.tanh(value));
				}
				else
				{
					value -= math.PI * math.acos(math.sqrt(value) * math.sign(value) * math.tanh(value));
				}
			}

			return value;
		}
	}
}