using System.Linq;
using System.Runtime.CompilerServices;
using NUnit.Framework;
using Unity.Burst;
using Unity.Mathematics;
using Unity.PerformanceTesting;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PerformanceTests
{
	[BurstCompile]
	public static unsafe class DistanceBenchmarks
	{
		const int ArraySize = 2500;
		const int IterationCount = 10;
		
		static void GetRandomPositions(float3[] positions)
		{
			for(int i = 0; i < positions.Length; i++)
			{
				positions[i] = Random.insideUnitSphere;
			}
		}
		
		static void GetRandomPositions(Vector3[] positions)
		{
			for(int i = 0; i < positions.Length; i++)
			{
				positions[i] = Random.insideUnitSphere;
			}
		}

		static float3 EmptyCall()
		{
			return float3.zero;
		}

		[BurstCompile]
		static void DistanceSqBurst(in float3 position, float3* positions, int length, out float3 result)
		{
			var closestPosition = float3.zero;
			var closestPositionDist = float.PositiveInfinity;

			for(int i = 0; i < length; i++)
			{
				var distance = math.distancesq(position, positions[i]);
				if(distance < closestPositionDist)
				{
					closestPositionDist = distance;
					closestPosition = positions[i];
				}
			}

			result = closestPosition;
		}
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[BurstCompile]
		static void DistanceSqBurstInlined(in float3 position, float3* positions, int length, out float3 result)
		{
			var closestPosition = float3.zero;
			var closestPositionDist = float.PositiveInfinity;

			for(int i = 0; i < length; i++)
			{
				var distance = math.distancesq(position, positions[i]);
				if(distance < closestPositionDist)
				{
					closestPositionDist = distance;
					closestPosition = positions[i];
				}
			}

			result = closestPosition;
		}
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[BurstCompile(FloatPrecision.Low, FloatMode.Fast)]
		static void DistanceSqBurstInlinedFastFloatMode(in float3 position, float3* positions, int length, out float3 result)
		{
			var closestPosition = float3.zero;
			var closestPositionDist = float.PositiveInfinity;

			for(int i = 0; i < length; i++)
			{
				var distance = math.distancesq(position, positions[i]);
				if(distance < closestPositionDist)
				{
					closestPositionDist = distance;
					closestPosition = positions[i];
				}
			}

			result = closestPosition;
		}
		
		[Test, Performance]
		public static void Dummy()
		{
			float3 result = float3.zero;
			float3 position = float3.zero;
			float3[] positions = new float3[ArraySize];

			Measure.Method(() =>
			       {
				       result = EmptyCall();
			       })
			       .SetUp(() =>
			       {
				       position = Random.insideUnitSphere;
				       GetRandomPositions(positions);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		[Test, Performance]
		public static void MathDistanceSqFloat3BurstInlineFastFloat()
		{
			float3 result = float3.zero;
			float3 position = float3.zero;
			float3[] positions = new float3[ArraySize];

			Measure.Method(() =>
			       {
				       fixed(float3* arrayPtr = positions)
				       {
					       DistanceSqBurstInlinedFastFloatMode(position, arrayPtr, ArraySize, out result);
				       }
			       })
			       .SetUp(() =>
			       {
				       position = Random.insideUnitSphere;
				       GetRandomPositions(positions);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		[Test, Performance]
		public static void MathDistanceSqFloat3BurstInline()
		{
			float3 result = float3.zero;
			float3 position = float3.zero;
			float3[] positions = new float3[ArraySize];

			Measure.Method(() =>
			       {
				       fixed(float3* arrayPtr = positions)
				       {
					       DistanceSqBurstInlined(position, arrayPtr, ArraySize, out result);
				       }
			       })
			       .SetUp(() =>
			       {
				       position = Random.insideUnitSphere;
				       GetRandomPositions(positions);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		[Test, Performance]
		public static void MathDistanceSqFloat3Burst()
		{
			float3 result = float3.zero;
			float3 position = float3.zero;
			float3[] positions = new float3[ArraySize];

			Measure.Method(() =>
			       {
				       fixed(float3* arrayPtr = positions)
				       {
					       DistanceSqBurst(position, arrayPtr, ArraySize, out result);
				       }
			       })
			       .SetUp(() =>
			       {
				       position = Random.insideUnitSphere;
				       GetRandomPositions(positions);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[Test, Performance]
		public static void MathDistanceSqFloat3()
		{
			float3 result = float3.zero;
			float3 position = float3.zero;
			float3[] positions = new float3[ArraySize];

			Measure.Method(() =>
			       {
				       var closestPosition = float3.zero;
				       var closestPositionDist = float.PositiveInfinity;

				       for(int i = 0; i < positions.Length; i++)
				       {
					       var distance = math.distancesq(position, positions[i]);
					       if(distance < closestPositionDist)
					       {
						       closestPositionDist = distance;
						       closestPosition = positions[i];
					       }
				       }

				       result = closestPosition;
			       })
			       .SetUp(() =>
			       {
				       position = Random.insideUnitSphere;
				       GetRandomPositions(positions);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		[Test, Performance]
		public static void MathDistanceVector3()
		{
			Vector3 result = Vector3.zero;
			Vector3 position = Vector3.zero;
			Vector3[] positions = new Vector3[ArraySize];

			Measure.Method(() =>
			       {
				       var closestPosition = Vector3.zero;
				       var closestPositionDist = float.PositiveInfinity;

				       for(int i = 0; i < positions.Length; i++)
				       {
					       var distance = math.distance(position, positions[i]);
					       if(distance < closestPositionDist)
					       {
						       closestPositionDist = distance;
						       closestPosition = positions[i];
					       }
				       }

				       result = closestPosition;
			       })
			       .SetUp(() =>
			       {
				       position = Random.insideUnitSphere;
				       GetRandomPositions(positions);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		[Test, Performance]
		public static void MathDistanceFloat3()
		{
			float3 result = float3.zero;
			float3 position = float3.zero;
			float3[] positions = new float3[ArraySize];

			Measure.Method(() =>
			       {
				       var closestPosition = Vector3.zero;
				       var closestPositionDist = float.PositiveInfinity;

				       for(int i = 0; i < positions.Length; i++)
				       {
					       var distance = math.distance(position, positions[i]);
					       if(distance < closestPositionDist)
					       {
						       closestPositionDist = distance;
						       closestPosition = positions[i];
					       }
				       }

				       result = closestPosition;
			       })
			       .SetUp(() =>
			       {
				       position = Random.insideUnitSphere;
				       GetRandomPositions(positions);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		[Test, Performance]
		public static void MathfDistanceVector3()
		{
			Vector3 result = Vector3.zero;
			Vector3 position = Vector3.zero;
			Vector3[] positions = new Vector3[ArraySize];

			Measure.Method(() =>
			       {
				       var closestPosition = Vector3.zero;
				       var closestPositionDist = float.PositiveInfinity;

				       for(int i = 0; i < positions.Length; i++)
				       {
					       var distance = Vector3.Distance(position, positions[i]);
					       if(distance < closestPositionDist)
					       {
						       closestPositionDist = distance;
						       closestPosition = positions[i];
					       }
				       }

				       result = closestPosition;
			       })
			       .SetUp(() =>
			       {
				       position = Random.insideUnitSphere;
				       GetRandomPositions(positions);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[Test, Performance]
		public static void MathfDistanceFloat3()
		{
			float3 result = float3.zero;
			float3 position = float3.zero;
			float3[] positions = new float3[ArraySize];

			Measure.Method(() =>
			       {
				       var closestPosition = float3.zero;
				       var closestPositionDist = float.PositiveInfinity;

				       for(int i = 0; i < positions.Length; i++)
				       {
					       var distance = Vector3.Distance(position, positions[i]);
					       if(distance < closestPositionDist)
					       {
						       closestPositionDist = distance;
						       closestPosition = positions[i];
					       }
				       }

				       result = closestPosition;
			       })
			       .SetUp(() =>
			       {
				       position = Random.insideUnitSphere;
				       GetRandomPositions(positions);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[Test, Performance]
		public static void LinqMathfDistance()
		{
			float3 result = float3.zero;
			float3 position = float3.zero;
			float3[] positions = new float3[ArraySize];

			Measure.Method(() =>
			       {
				       result = positions.OrderBy(pos => Vector3.Distance(pos, position)).First();
			       })
			       .SetUp(() =>
			       {
				       position = Random.insideUnitSphere;
				       GetRandomPositions(positions);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
	}
}