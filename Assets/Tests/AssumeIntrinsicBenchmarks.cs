using System.Diagnostics;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.PerformanceTesting;
using Random = Unity.Mathematics.Random;
using static Unity.Burst.CompilerServices.Hint;

namespace PerformanceTests
{
	[BurstCompile]
	public static class AssumeIntrinsicBenchmarks
	{
		const int IterationCount = 1000;
		const int ArrayLength = 10000;

		[Test, Performance]
		public static void ArrayAccessDefaultBenchmark()
		{
			NativeArray<float> array = default;

			Measure.Method(() =>
			       {
				       unsafe
				       {
					       array = new NativeArray<float>(ArrayLength, Allocator.Temp);
					       var random = new Random((uint)Stopwatch.GetTimestamp());
					       var unsafePtr = (float*)array.GetUnsafePtr();
					       ArrayAccessDefault(ref random, unsafePtr, ArrayLength);
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .CleanUp(() => { array.Dispose(); })
			       .Run();
		}
		
		[Test, Performance]
		public static void ArrayAccessAssumeBenchmark()
		{
			NativeArray<float> array = default;

			Measure.Method(() =>
			       {
				       unsafe
				       {
					       array = new NativeArray<float>(ArrayLength, Allocator.Temp);
					       var random = new Random((uint)Stopwatch.GetTimestamp());
					       var unsafePtr = (float*)array.GetUnsafePtr();
					       ArrayAccessAssume(ref random, unsafePtr, ArrayLength);
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .CleanUp(() => { array.Dispose(); })
			       .Run();
		}
		
		[BurstCompile]
		static unsafe void ArrayAccessAssume(ref Random random, float* ptr, int length)
		{
			for(int i = 0; i < length; i++)
			{
				var value = random.NextFloat();
				
				// Does giving this Hint help us in any way?
				Assume(value >= 0f);

				if(value >= 0f)
				{
					ptr[i] = random.NextFloat();
				}
			}
		}

		[BurstCompile]
		static unsafe void ArrayAccessDefault(ref Random random, float* ptr, int length)
		{
			for(int i = 0; i < length; i++)
			{
				var value = random.NextFloat();
				if(value >= 0f)
				{
					ptr[i] = random.NextFloat();
				}
			}
		}
	}
}