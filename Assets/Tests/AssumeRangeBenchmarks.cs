using NUnit.Framework;
using Unity.Burst;
using Unity.Burst.CompilerServices;
using Unity.Jobs;
using Unity.PerformanceTesting;

namespace PerformanceTests
{
	[BurstCompile]
	public static class AssumeRangeBenchmarks
	{
		[BurstCompile]
		struct LengthJob : IJob
		{
			public int Length;
			public bool Result;

			public void Execute()
			{
				for(int i = 0; i < 1000000; i++)
				{
					Result = IsLengthNegative();
				}
			}

			public int GetLength()
			{
				return Length;
			}

			public bool IsLengthNegative()
			{
				return GetLength() < 0;
			}
		}

		[BurstCompile]
		struct LengthIntrinsicsJob : IJob
		{
			public int Length;
			public bool Result;

			public void Execute()
			{
				for(int i = 0; i < 1000000; i++)
				{
					Result = IsLengthNegativeIntrinsics();
				}
			}

			[return: AssumeRange(0, int.MaxValue)]
			public int GetLengthIntrinsics()
			{
				return Length;
			}

			public bool IsLengthNegativeIntrinsics()
			{
				return GetLengthIntrinsics() < 0;
			}
		}

		[Test, Performance]
		public static void MeasureGetLengthIntrinsics()
		{
			Measure.Method(() =>
			       {
				       new LengthIntrinsicsJob
				       {
					       Length = 10
				       }.Run();
			       })
			       .WarmupCount(10)
			       .MeasurementCount(100)
			       .IterationsPerMeasurement(10)
			       .Run();
		}

		[Test, Performance]
		public static void MeasureGetLength()
		{
			Measure.Method(() =>
			       {
				       new LengthJob
				       {
					       Length = 10
				       }.Run();
			       })
			       .WarmupCount(10)
			       .MeasurementCount(100)
			       .IterationsPerMeasurement(10)
			       .Run();
		}
	}
}