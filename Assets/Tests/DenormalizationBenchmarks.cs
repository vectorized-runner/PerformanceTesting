using NUnit.Framework;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.PerformanceTesting;
using Unity.Transforms;
using Random = UnityEngine.Random;

namespace PerformanceTests
{
	public static class DenormalizationBenchmarks
	{
		struct Target : IComponentData
		{
			public int Priority;
		}

		struct Dead : IComponentData
		{
		}

		struct Race : IComponentData
		{
			public int Value;
		}

		struct AttackerDenormalized : IComponentData
		{
			public float3 TargetPosition;
			public Entity Target;
			public bool ShouldMove;
			public float MoveSpeed;
		}

		struct Attacker : IComponentData
		{
			public Entity Target;
			public float MoveSpeed;
		}

		[DisableAutoCreation]
		class AttackerMoveTowardsTargetSystem : SystemBase
		{
			protected override void OnUpdate()
			{
				var deadData = GetComponentDataFromEntity<Dead>(true);
				var raceData = GetComponentDataFromEntity<Race>(true);
				var targetData = GetComponentDataFromEntity<Target>(true);
				var translationData = GetComponentDataFromEntity<Translation>(true);
				var dt = Time.DeltaTime;

				Entities.ForEach((ref Attacker attacker, ref Translation translation) =>
				        {
					        var target = attacker.Target;

					        if(!deadData.HasComponent(target) && raceData[target].Value == 0 &&
						        targetData[target].Priority == 0)
					        {
						        translation.Value +=
							        math.normalizesafe(translationData[target].Value - translation.Value) *
							        attacker.MoveSpeed * dt;
					        }
				        })
				        .WithReadOnly(deadData)
				        .WithReadOnly(translationData)
				        .WithReadOnly(raceData)
				        .WithReadOnly(deadData)
				        .Run();
			}
		}

		[DisableAutoCreation]
		class DenormalizedAttackerMoveTowardsTargetSystem : SystemBase
		{
			protected override void OnUpdate()
			{
				var deadData = GetComponentDataFromEntity<Dead>(true);
				var raceData = GetComponentDataFromEntity<Race>(true);
				var targetData = GetComponentDataFromEntity<Target>(true);
				var translationData = GetComponentDataFromEntity<Translation>(true);

				// Check if should move towards
				// Record Target position
				Entities.ForEach((ref AttackerDenormalized attackerDenormalized) =>
				        {
					        var target = attackerDenormalized.Target;
					        attackerDenormalized.ShouldMove = !deadData.HasComponent(target) &&
						        raceData[target].Value == 0 && targetData[target].Priority == 0;
					        attackerDenormalized.TargetPosition = translationData[target].Value;
				        })
				        .WithReadOnly(deadData)
				        .WithReadOnly(translationData)
				        .WithReadOnly(raceData)
				        .WithReadOnly(deadData)
				        .Run();

				var dt = Time.DeltaTime;

				// Move Towards target
				Entities.ForEach((ref AttackerDenormalized attackerDenormalized, ref Translation translation) =>
				        {
					        if(!attackerDenormalized.ShouldMove)
						        return;

					        translation.Value +=
						        math.normalizesafe(attackerDenormalized.TargetPosition - translation.Value) *
						        attackerDenormalized.MoveSpeed * dt;
				        })
				        .Run();
			}
		}

		[Test, Performance]
		public static void MeasureDenormalizedSystem()
		{
			World world = default;

			Measure.Method(() =>
			       {
				       // Tick 10 times
				       for(int i = 0; i < 10; i++)
				       {
					       ECSTestUtil.TickWorld(world);
				       }
			       })
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       ECSTestUtil.AddSystemToWorld<DenormalizedAttackerMoveTowardsTargetSystem>(world);

				       // 1k Attackers and 1k Targets
				       var em = world.EntityManager;
				       var attackerArchetype = em.CreateArchetype(typeof(AttackerDenormalized), typeof(Translation));
				       var targetArchetype = em.CreateArchetype(typeof(Target), typeof(Race), typeof(Translation));
				       var spawnCount = 1000;

				       using var attackers = em.CreateEntity(attackerArchetype, spawnCount, Allocator.Temp);
				       using var targets = em.CreateEntity(targetArchetype, spawnCount, Allocator.Temp);
				       using var ecb = new EntityCommandBuffer(Allocator.Temp, PlaybackPolicy.SinglePlayback);

				       for(int i = 0; i < spawnCount; i++)
				       {
					       ecb.SetComponent(targets[i], new Translation
					       {
						       Value = Random.insideUnitSphere,
					       });
					       ecb.SetComponent(targets[i], new Race
					       {
						       Value = Random.value > 0.5f ? 1 : 0,
					       });
					       ecb.SetComponent(targets[i], new Target
					       {
						       Priority = Random.value > 0.25f ? 1 : 0,
					       });

					       if(Random.value > 0.5f)
					       {
						       ecb.AddComponent<Dead>(targets[i]);
					       }
				       }

				       for(int i = 0; i < spawnCount; i++)
				       {
					       ecb.SetComponent(attackers[i], new AttackerDenormalized
					       {
						       MoveSpeed = 2.5f,
						       Target = targets[Random.Range(0, spawnCount)]
					       });
				       }

				       ecb.Playback(em);
			       })
			       .CleanUp(() => { world.Dispose(); })
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void DefaultAttackerMoveTowardsTargetSystem()
		{
			World world = default;

			Measure.Method(() =>
			       {
				       // Tick 10 times
				       for(int i = 0; i < 10; i++)
				       {
					       ECSTestUtil.TickWorld(world);
				       }
			       })
			       .SetUp(() =>
			       {
				       world = ECSTestUtil.CreateEditorWorld();
				       ECSTestUtil.AddSystemToWorld<AttackerMoveTowardsTargetSystem>(world);

				       // 1k Attackers and 1k Targets
				       var em = world.EntityManager;
				       var attackerArchetype = em.CreateArchetype(typeof(Attacker), typeof(Translation));
				       var targetArchetype = em.CreateArchetype(typeof(Target), typeof(Race), typeof(Translation));
				       var spawnCount = 1000;

				       using var attackers = em.CreateEntity(attackerArchetype, spawnCount, Allocator.Temp);
				       using var targets = em.CreateEntity(targetArchetype, spawnCount, Allocator.Temp);
				       using var ecb = new EntityCommandBuffer(Allocator.Temp, PlaybackPolicy.SinglePlayback);

				       for(int i = 0; i < spawnCount; i++)
				       {
					       ecb.SetComponent(targets[i], new Translation
					       {
						       Value = Random.insideUnitSphere,
					       });
					       ecb.SetComponent(targets[i], new Race
					       {
						       Value = Random.value > 0.5f ? 1 : 0,
					       });
					       ecb.SetComponent(targets[i], new Target
					       {
						       Priority = Random.value > 0.25f ? 1 : 0,
					       });

					       if(Random.value > 0.5f)
					       {
						       ecb.AddComponent<Dead>(targets[i]);
					       }
				       }

				       for(int i = 0; i < spawnCount; i++)
				       {
					       ecb.SetComponent(attackers[i], new Attacker
					       {
						       MoveSpeed = 2.5f,
						       Target = targets[Random.Range(0, spawnCount)]
					       });
				       }

				       ecb.Playback(em);
			       })
			       .CleanUp(() => { world.Dispose(); })
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}
	}
}