using System.Diagnostics;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.PerformanceTesting;
using Random = Unity.Mathematics.Random;

namespace PerformanceTests
{
	[BurstCompile]
	public static unsafe class RandomBenchmarks
	{
		const int ArraySize = 10000;
		const int IterationCount = 30;

		[Test, Performance]
		public static void GetUnityRandom()
		{
			Measure.Method(() =>
			       {
				       var myArray = new float[ArraySize];

				       for(int i = 0; i < myArray.Length; i++)
				       {
					       myArray[i] = UnityEngine.Random.value;
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[Test, Performance]
		public static void GetMathRandom()
		{
			Measure.Method(() =>
			       {
				       var myArray = new float[ArraySize];
				       var random = new Random((uint)Stopwatch.GetTimestamp());

				       for(int i = 0; i < myArray.Length; i++)
				       {
					       myArray[i] = random.NextFloat();
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[Test, Performance]
		public static void GetMathRandomBurst()
		{
			Measure.Method(() =>
			       {
				       var myArray = new float[ArraySize];
				       var random = new Random((uint)Stopwatch.GetTimestamp());

				       fixed(float* arrayPtr = myArray)
				       {
					       MathRandomBurst(ref random, arrayPtr, ArraySize);
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[Test, Performance]
		public static void GetMathRandomBurstStackAlloc()
		{
			Measure.Method(() =>
			       {
				       float* myArray = stackalloc float[ArraySize];
				       var random = new Random((uint)Stopwatch.GetTimestamp());

				       MathRandomBurst(ref random, myArray, ArraySize);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[Test, Performance]
		public static void GetMathRandomBurstNative()
		{
			NativeArray<float> myArray = default;

			Measure.Method(() =>
			       {
				       myArray = new NativeArray<float>(ArraySize, Allocator.Temp);
				       var random = new Random((uint)Stopwatch.GetTimestamp());
				       var arrayPtr = (float*)myArray.GetUnsafePtr();
				       MathRandomBurst(ref random, arrayPtr, ArraySize);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .CleanUp(() => { myArray.Dispose(); })
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		[Test, Performance]
		public static void GetMathRandomBurstNative_2()
		{
			NativeArray<float> myArray = default;

			Measure.Method(() =>
			       {
				       myArray = new NativeArray<float>(ArraySize, Allocator.Temp);
				       var random = new Random((uint)Stopwatch.GetTimestamp());
				       MathRandomBurst(ref random, ref myArray);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .CleanUp(() => { myArray.Dispose(); })
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		[BurstCompile]
		static void MathRandomBurst(ref Random random, ref NativeArray<float> array)
		{
			for(int i = 0; i < array.Length; i++)
			{
				array[i] = random.NextFloat();
			}
		}

		[BurstCompile]
		static void MathRandomBurst(ref Random random, float* array, int arrayLength)
		{
			for(int i = 0; i < arrayLength; i++)
			{
				array[i] = random.NextFloat();
			}
		}
	}
}