using NUnit.Framework;
using Unity.Burst;
using Unity.Burst.CompilerServices;
using Unity.Collections;
using Unity.PerformanceTesting;

namespace PerformanceTests
{
	[BurstCompile]
	public static class AllocationBenchmarks
	{
		const int ArraySize = 100;
		const int IterationCount = 100000;

		[Test, Performance]
		public static void AllocateNativeArrayUninitialized()
		{
			NativeArray<int> arr = default;

			Measure.Method(() =>
			       {
				       int value = AllocateUninitialized(out arr);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .CleanUp(() => arr.Dispose())
			       .Run();
		}

		[Test, Performance]
		public static void AllocateNativeArrayClear()
		{
			NativeArray<int> arr = default;

			Measure.Method(() =>
			       {
				       int value = AllocateClear(out arr);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .CleanUp(() => arr.Dispose())
			       .Run();
		}

		[Test, Performance]
		public static void AllocateStackAllocSkipLocalsInit()
		{
			Measure.Method(() =>
			       {
				       int elem = StackAllocBurstSkipLocalsInit();
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[Test, Performance]
		public static void AllocateStackAlloc()
		{
			Measure.Method(() =>
			       {
				       int elem = StackAllocDefault();
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[Test, Performance]
		public static void AllocateStackAllocNoBurst()
		{
			Measure.Method(() =>
			       {
				       int elem = StackAllocDefaultNoBurst();
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		
		[Test, Performance]
		public static void AllocateManaged()
		{
			Measure.Method(() =>
			       {
				       int elem = AllocateManagedMethod();
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		static int AllocateManagedMethod()
		{
			var arr = new int[100];
			return arr[0];
		}

		static unsafe int StackAllocDefaultNoBurst()
		{
			var arr = stackalloc int[ArraySize];
			return arr[0];
		}

		[BurstCompile]
		static unsafe int StackAllocDefault()
		{
			var arr = stackalloc int[ArraySize];
			return arr[0];
		}

		[BurstCompile]
		[SkipLocalsInit]
		static unsafe int StackAllocBurstSkipLocalsInit()
		{
			var arr = stackalloc int[ArraySize];
			return arr[0];
		}

		static int AllocateUninitialized(out NativeArray<int> arr)
		{
			arr = new NativeArray<int>(ArraySize, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
			return arr[0];
		}

		static int AllocateClear(out NativeArray<int> arr)
		{
			arr = new NativeArray<int>(ArraySize, Allocator.Temp, NativeArrayOptions.ClearMemory);
			return arr[0];
		}
	}
}